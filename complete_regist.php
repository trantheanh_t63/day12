<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="style.css">
</head>

<?php

error_reporting(E_ERROR | E_PARSE);

function console_log($data)
{
    $output = json_encode($data);

    echo "<script>console.log('{$output}' );</script>";
}

?>

<body>
    <div class="body">
        <div class='form text-align-center'>
            <p>
                Bạn đã đăng ký thành công sinh viên
            </p>
            <a class="mt-25 black" href="management.php">Quay lại danh sách sinh viên</a>
        </div>
    </div>
</body>

</html>