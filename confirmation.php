<!DOCTYPE html>
<html>

<head>
    <link rel="stylesheet" href="style.css">
</head>

<?php

error_reporting(E_ERROR | E_PARSE);

class Student
{
    public $name;
    public $gender;
    public $faculty;
    public $dob;
    public $address;
    public $picture;
}

function console_log($data)
{
    $output = json_encode($data);

    echo "<script>console.log('{$output}' );</script>";
}

$genders = array(
    "2" => "Nam",
    "1" => "Nữ"
);

$faculties = array(
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
);

session_start();
$input = new Student;
if (isset($_SESSION['input'])) {
    $input = $_SESSION['input'];
}

if (isset($_POST['confirm'])) {
    console_log($input);

    $host = "localhost";
    $dbname = "day12_db";
    $username = "root";
    $password = "";

    $conn = mysqli_connect($host, $username, $password, $dbname);

    if (mysqli_connect_errno()) {
        die("Error: " . mysqli_connect_error());
    }

    $sql = "INSERT INTO student (name, gender, faculty, birthday, address, avatar) 
            VALUES (?, ?, ?, ?, ?, ?)";

    $stmt = mysqli_stmt_init($conn);

    if (!mysqli_stmt_prepare($stmt, $sql)) {
        die(mysqli_error($conn));
    }

    $gender = (int) $input->gender - 1;
    $birthday = DateTime::createFromFormat('d/m/Y', $input->dob);
    $birthday = $birthday->format('Y-m-d H:i:s');

    mysqli_stmt_bind_param(
        $stmt,
        "sissss",
        $input->name,
        $gender,
        $input->faculty,
        $birthday,
        $input->address,
        $input->picture
    );
    mysqli_stmt_execute($stmt);

    header('Location: complete_regist.php');

    session_destroy();
}

?>

<body>
    <div class="body">
        <div class='form'>
            <form method="post">
                <div class="flex-start">
                    <div for="name" class="label">
                        Họ và tên
                    </div>
                    <div class="text ml-20">
                        <?= $input->name ?>
                    </div>
                </div>

                <div class="flex-start">
                    <div class="label">
                        Giới tính
                    </div>
                    <div class="text ml-20">
                        <?= $genders[$input->gender] ?>
                    </div>
                </div>

                <div class="flex-start">
                    <div class="label">
                        Phân khoa
                    </div>
                    <div class="text ml-20">
                        <?= $faculties[$input->faculty] ?>
                    </div>
                </div>

                <div class="flex-start">
                    <div class="label">
                        Ngày sinh
                    </div>
                    <div class="text ml-20">
                        <?= $input->dob ?>
                    </div>
                </div>

                <div class="flex-start">
                    <div class="label">
                        Địa chỉ
                    </div>
                    <div class="text ml-20">
                        <?= $input->address ?>
                    </div>
                </div>

                <div class="flex-start">
                    <div class="label mh-25">
                        Hình ảnh
                    </div>
                    <img class="ml-20" src="<?php echo $input->picture ?>" alt="picture" />
                </div>

                <div class="flex-center mt-50">
                    <input class="btn" type='submit' name="confirm" value="Xác nhận">
                </div>
            </form>
        </div>
    </div>
</body>

</html>